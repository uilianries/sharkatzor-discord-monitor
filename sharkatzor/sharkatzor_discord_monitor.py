import discord
import logging
import configparser
import argparse
import os

LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.DEBUG)
S_HANDLER = logging.StreamHandler()
S_HANDLER.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s [%(levelname)s] %(message)s')
S_HANDLER.setFormatter(formatter)
LOGGER.addHandler(S_HANDLER)


D_HANDLER = logging.StreamHandler()
D_HANDLER.setLevel(logging.ERROR)
D_HANDLER.setFormatter(formatter)


class SharkatzorDiscordMonitor(discord.Client):
    def __init__(self, *args, **kwargs):
        intents = discord.Intents.default()
        intents.message_content = True
        super().__init__(intents=intents)

        self.logger = LOGGER
        self.logger.info('Starting ...')
        config = self._load_config(kwargs['config_path'])
        self.general_channel = config['discord']['general_channel']
        self.maintenance_channel = config['discord']['maintenance_channel']
        self.public_channel = config['discord']['public_channel']
        self.allowed_roles = [int(it) for it in config['discord']['allowed_roles'].split(',')]
        self.allowed_users = [int(it) for it in config['discord']['allowed_users'].split(',')]
        self.discord_token = config['discord']['token']

        self.logger.info('Discord Token: {}****'.format(self.discord_token[:4]))
        self.logger.info('General Discord channel: {}****'.format(self.general_channel[:4]))
        self.logger.info('Private Discord channel: {}****'.format(self.maintenance_channel[:4]))
        self.logger.info('Shared  Discord channel: {}****'.format(self.public_channel[:4]))
        self.logger.info(f'Allowed Discord users: {self.allowed_users}')
        self.logger.info(f'Allowed Discord roles: {self.allowed_roles}')

    def _load_config(self, config_path):
        config = configparser.ConfigParser()
        config.read(config_path)
        return config

    async def on_ready(self):
        self.logger.info(f'We have logged in as {self.user}')
        self.channel = self.get_channel(int(self.general_channel))
        self.private_channel = self.get_channel(int(self.maintenance_channel))
        self.shared_channel = self.get_channel(int(self.public_channel))
        self.logger.info(f"Started as `{self.user}`.")

    async def on_message(self, message):
        await self._remove_twitch_message(message)

    async def on_message_edit(self, _, message):
        await self._remove_twitch_message(message)

    async def _remove_twitch_message(self, message):
        self.logger.debug(f"Message from {message.author.name}")
        if message.embeds:
            self.logger.debug(f"Message from {message.author.name} has embeds")
            if message.author.id not in self.allowed_users and not any(role.id in self.allowed_roles for role in message.author.roles):
                if message.channel.id == int(self.general_channel):
                    for embed in message.embeds:
                        if ("//www.twitch.tv/" in embed.url or "//twitch.tv/" in embed.url) and "twitch.tv/tomahawk_aoe" not in embed.url:
                            self.logger.warning(f"Delete message - #{message.author.name}: {message.content}")
                            await message.delete()
                            await self.channel.send(f"{message.author.mention} favor utilizar o canal {self.shared_channel.mention} para postar link da Twitch.")
                            return
            phishing_links = ["free distribution of discord nitro", "free nitro"]
            for phishing_link in phishing_links:
                if phishing_link in str(message.content).lower():
                    self.logger.warning(f"Delete message - #{message.author.name}: {message.content}")
                    await message.delete()
                    await message.author.kick("Usuário caiu no golpe do litrão e enviou phising no servidor.")
                    await self.private_channel.send(f"Usuário {message.author.mention} caiu no golpe do litrão. Mensagem removida e usuário kickado.")
                    break


def main():
    home_folder = os.path.expanduser('~')
    default_config_path = os.path.join(home_folder, 'Development', 'sharkatzor-discord-monitor', 'conf.ini')
    parser = argparse.ArgumentParser(description='Sharkatzor Discord Monitor')
    parser.add_argument('--config', '-c', help='Configuration file', default=default_config_path)
    args = parser.parse_args()
    client = SharkatzorDiscordMonitor(config_path=args.config)
    client.run(client.discord_token, log_handler=D_HANDLER)


if __name__ == "__main__":
    main()